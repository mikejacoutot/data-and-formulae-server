from subprocess import call
import os

def Latex2Png(equation,location=''):
    
    if location:
        url=location 
    else:
        url='/Users/mike/latex'
    if not os.isDir:
        os.makedirs(url)

    fileName='doesitwork'
    contents=r'''\documentclass[preview]{standalone}
    \begin{document}
    \begin{equation*}'''+equation+r'''\end{equation*}
    \end{document}'''
    print contents
    text_file = open(url+fileName+'.tex', "w")
    text_file.write(contents)
    text_file.close()
    call(['pdflatex', url+fileName+'.tex'])
    call(['convert' ,'-density', '600', url+fileName+'.pdf', '-quality', '90' ,url+fileName+'.png'])
    return location+fileName
