from django.conf.urls import patterns, include, url
from django.contrib import admin
import daf.admin
urlpatterns = patterns('',
    url(r'^reload','getequations.getFromBook'),
    url(r'^feedback','getequations.sendFeedback'),
    url(r'^solveEquation','daf.views.solveEquation'),
    url(r'^do','daf.views.do'),
    url(r'^getSteamValues','daf.views.getSteamValues'),
    url(r'^sync','daf.views.sync'),
    url(r'^acknowledgement','daf.views.acknowledgement'),
    url(r'^support','daf.views.support'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^selectable/', include('selectable.urls')),
)
