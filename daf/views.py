from equation.models import Formula, FeedBack,UnitCategory,Unit,Symbol,Image,Ack,SteamTable,FormulaCategory,Graph,GraphAxis
from collections import defaultdict
from django.http import HttpResponse
import sympy
from sympy.parsing.sympy_parser import parse_expr
import json
from django.views.decorators.csrf import csrf_exempt
from dateutil.parser import parse
from datetime import datetime
from django.core.cache import cache
from iapws import IAPWS97 as steam
from django.http import HttpResponse

def acknowledgement(request):
    ack=Ack.objects.get(pk=1).text
    return HttpResponse(json.dumps({"text":ack}),status=200, content_type="application/json")





@csrf_exempt
def getSteamValues(request):
    print request.POST
    vars={}
    for k,v in request.POST.items():
        if k.strip(' ')=='x':
            if v>1:
                v=1
        vars[k.strip(' ')]=float(v)
    try:
        success=True
        anstemp=steam(**vars)
        ans={}
        for k,v in anstemp.__dict__.iteritems():
            if v:
                ans[k]=v
    except NotImplementedError:
        success=False
        ans='' 
    resp={'solution':ans,
          'success':success }
    return HttpResponse(json.dumps(resp),status=200, content_type="application/json")
    

@csrf_exempt
def solveEquation(request):
    initialTime=datetime.now()
    print request
    EquationID=request.POST.get('formulaID')
    print 'formulaID', EquationID
    for i in Formula.objects.all():
        print i.pk
    formula=Formula.objects.get(pk=EquationID)
    equation=formula.sympy_equation
    required_variables=formula.getVariables()
    variables={}
    print 'a',datetime.now()-initialTime
    for v in required_variables:
        if request.POST.get(v):
            variables[v]=request.POST.get(v)
        else:
            variables[v]=None
    #variables={'y':1,'m':2,'c':-2,'x':None}
    print 'b',datetime.now()-initialTime
    if not variables:
        resp={'error':'no variables'}
        return HttpResponse(json.dumps(resp),status=200, content_type="application/json")
    if len(variables)!=len(required_variables):
        resp={'error':'not enough variables'}
        return HttpResponse(json.dumps(resp),status=200, content_type="application/json")
    print 'c',datetime.now()-initialTime    
    resp={}
    #cacheAns=cache.get()
    print variables
    answers=solveFor(variables,equation)
    validAnswers=[]
    print 'd',datetime.now()-initialTime
    for answer in answers:

        if not((not answer[1].is_number) or (str(answer[1])=='nan')):
            validAnswers.append(answer)
        
    print 'e',datetime.now()-initialTime
    if len(validAnswers)==0:
        resp={'error':'no solutions found'}
        return HttpResponse(json.dumps(resp),status=200, content_type="application/json")
    
    print 'f',datetime.now()-initialTime
    resp['answers']={}

    for i,v in enumerate(validAnswers):
        real,imag=v[1].as_real_imag()
        resp['answers'][str(i)]={'solution':v[0],
                                'complex':True,
                                'real':str(real),
                                'imaginary':str(imag),
                                'vars':str(*v[2])}
            
    resp['number of solutions']=len(resp['answers'])
    print 'response is',resp
    return HttpResponse(json.dumps(resp),status=200, content_type="application/json")

def iterSync(d,model):
    l=[]
    for o in model.objects.all():
        if o.pk in d:
            if o.modified > d[o.pk]:
                l.append(o.syncDict())
            del d[o.pk]
        else:
            l.append(o.syncDict())
    for o in d:
        l.append({'pk':o,'delete':True})
    return l

@csrf_exempt    
def sync(request):
    d=defaultdict(dict)
    for k,v in request.POST.iteritems():
        t,p=k.rstrip(']').split('[')
        d[t][int(p)]=parse(v)
    resp={}

    cs=d.get('Categories') or {}
    resp['Categories']=iterSync(cs,FormulaCategory)

    fs=d.get('Formulae') or {}
    resp['Formulae']=iterSync(fs,Formula)

    ss=d.get('Symbols') or {}    
    resp['Symbols']=iterSync(ss,Symbol)

    ucs=d.get('UnitCategories') or {}    
    resp['UnitCategories']=iterSync(ucs,UnitCategory)

    us=d.get('Units') or {}    
    resp['Units']=iterSync(us,Unit)

    us=d.get('Images') or {}    
    resp['Images']=iterSync(us,Image)

    gs=d.get('Graphs') or {}
    resp['Graphs']=iterSync(gs,Graph)

    ga=d.get('GraphAxes') or {}
    resp['GraphAxes']=iterSync(ga,GraphAxis)

    sts=d.get('Steam') or {}
    resp['Steam']=iterSync(sts,SteamTable)
    
    return HttpResponse(json.dumps(resp),status=200, content_type="application/json")
    
def do(request):
    text='''base
    			Length & metre & m \\
    			Mass & kilogram & kg \\
    			Time & second & s \\
    			Electric current & ampere & A \\
    			Thermodynamic temperature & kelvin & K \\
    			Luminous intensity & candela & cd \\

    derived
    			Acceleration, linear & $\text{metre}/\text{second}^2$ & \SI{}{m.s^{-2}} \\
    			Acceleration, angular & $\text{radian}/\text{second}^2$ & \SI{}{rad.s^{-2}} \\
    			Area & $\text{metre}^2$ & \SI{}{m^2} \\
    			Density & $\text{kilogram} / \text{metre}^3$ & \SI{}{kg.m^{-3}} \\
    			Force & newton & N (= \SI{}{kg.m.s^{-2}})\\
    			Frequency & hertz & (Hz = \SI{}{s^{-1}}) \\
    			Impulse, linear & newton-second & \SI{}{N.s} \\
    			Impulse, angular & newton-metre-second & \SI{}{N.m.s} \\
    			Moment of force & newton-metre & \SI{}{N.m} \\
    			Second moment of area & $\text{metre}^4$ & \SI{}{m^4} \\
    			Moment of inertia &  $\text{kilogram-metre}^2$ & \SI{}{kg.m^2} \\
    			Momentum, linear & $\text{kilogram-metre}/\text{second}$ & \SI{}{kg.m.s^{-1}} \\
    			Momentum, angular & $\text{kilogram-metre}^2/\text{second}$ & \SI{}{kg.m^2.s^{-1}} \\
    			Power & watt & W (= \SI{}{J.s^{-1}} = \SI{}{N.m.s^{-1}} \\
    			Pressure, stress & pascal & Pa (= \SI{}{N.m^{-2}}) \\
    			Stiffness (linear), spring constant & $\text{newton}/\text{metre}$ & \SI{}{N.m^{-1}} \\
    			Velocity, linear & $\text{metre}/\text{second}$ & \SI{}{m.s^{-1}} \\
    			Velocity, angular & $\text{radian}/\text{second}$ & \SI{}{rad.s^{-1}} \\
    			Volume & $\text{metre}^3$ & \SI{}{m^3} \\
    			Work, energy & joule & J (= \SI{}{N.m})\\

    electric
    			Potential & volt & V (= \SI{}{W.A^{-1}}) \\
    			Resistance & ohm & $\upomega$ (= \SI{}{V.A^{-1}})\\
    			Charge & coulomb & C (= \SI{}{A.s}) \\
    			Capacitance & farad & F (= \SI{}{A.s.V^{-1}}) \\
    			Electric field strength & $\text{volt}/\text{metre}$ & \SI{}{V.m^{-1}} \\
    			Electric flux density & $\text{coulomb}/\text{metre}^2$ & \SI{}{C.m^{-2}} \\
			
    magnetic
                Magnetic flux & weber & Wb (= \SI{}{V.s}) \\
    			Inductance & henry & H (= \SI{}{V.s.A^{-1}}) \\
    			Magnetic field strength & --- &  \SI{}{A.m^{-1}}  \\
    			Magnetic flux density & --- & \SI{}{Wb.m^{-2}} \\
    '''
    unitsDict={}
    for i in text.splitlines(): 
    
        if i:
        
            buf= i.replace('\t','').replace('\\','').split('&')
            if len(buf)==3:
                unitsDict[buf[0]]={'quantity':buf[1],'symbol':buf[2]}
    for i in unitsDict.keys():
        uc=UnitCategory(name=' '.join(list(reversed(i.lstrip().rstrip().split(',')))).title().lstrip().rstrip())
        uc.save()
    resp=['Success']
    return HttpResponse(json.dumps(resp),status=200, content_type="application/json")

def solveFor(variables,equation):
    initialTime=datetime.now()
    print '1',datetime.now()-initialTime
    eq=equation.replace('^','**').split('=')
    l=parse_expr(eq[0])
    r=parse_expr(eq[1])
    exp=sympy.Eq(l,r)
    print exp
    print '2',datetime.now()-initialTime
    
    print variables
    for key,var in variables.iteritems():
        print 'kv',key,var   
        if not var:
            print 'not var'
            equation_cache_id=str(equation)+','+str(key)
            cacheAns=cache.get(equation_cache_id)
            if not cacheAns:
                print 'key',exp,key
                ans=sympy.solve(exp,key)
                cache.set(equation_cache_id,ans)
            else:
                ans=cacheAns
            print 'ans',ans
    print '3',datetime.now()-initialTime
    sols=[]
    print '4',datetime.now()-initialTime
    for eq in ans:
        print 'eqn',eq
        vars={}
        l=[]
        for k,v in variables.iteritems():
            if v:
                vars[k]=v
                l.append(k)
        var=[x for x in variables.keys() if x not in l]
        sols.append((str(eq),eq.subs(vars).evalf(),var))
    print sols
    print '5',datetime.now()-initialTime
    return sols
    
    
def support(request):
    html = "<html><body>email pruthvikar@gmail.com for support</body></html>"
    return HttpResponse(html)
