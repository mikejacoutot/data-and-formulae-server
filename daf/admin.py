from equation.models import Formula,UnitCategory,Unit,Symbol,Image,FeedBack, Ack,SteamTable,SteamParameter,SteamCalculation,CompoundCategoryContainer,CompoundCategory,CompoundCategoryRow,CompoundConversion, FormulaCategory, CategoryAlias,GraphAxis,Graph,GraphContainer
from django.conf import settings
from django.contrib import admin
from django import forms
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe
from django.forms import TextInput, Textarea,CheckboxInput,NumberInput
from django.db import models
from django.forms.models import BaseInlineFormSet
#from nested_inlines.admin import NestedModelAdmin, NestedStackedInline, NestedTabularInline

####### IMAGE #######

class AdminImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):
        output = []
        if value and getattr(value, "url", None):
            image_url = value.url
            file_name=str(value)
            output.append(u' <a href="%s" target="_blank"><img src="%s" alt="%s" /></a>' % \
                (image_url, image_url, file_name))
        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))

class ImageInlineFormula(admin.StackedInline):
    model = Image
    extra=1
    fields=('latex','main','img','name',)
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'img':
            request = kwargs.pop("request", None)
            kwargs['widget'] = AdminImageWidget
            return db_field.formfield(**kwargs)
        return super(ImageInlineFormula,self).formfield_for_dbfield(db_field, **kwargs)

class ImageInlineGraph(admin.StackedInline):
    max_num=1 
    model = Image
    extra=1
    fields=('latex','img',)
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'img':
            request = kwargs.pop("request", None)
            kwargs['widget'] = AdminImageWidget
            return db_field.formfield(**kwargs)
        return super(ImageInlineGraph,self).formfield_for_dbfield(db_field, **kwargs)

class ImageAdmin(admin.ModelAdmin):
    model=Image
    fields=('latex','main','img','name')
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'img':
            request = kwargs.pop("request", None)
            kwargs['widget'] = AdminImageWidget
            return db_field.formfield(**kwargs)
        return super(ImageAdmin,self).formfield_for_dbfield(db_field, **kwargs)
    list_display=['name','formula','graph']


####### GRAPH #######

class GraphAxisInline(admin.StackedInline):
    model=GraphAxis
    fields=('title',('axis_type','logarithmic'),('category','unit'),('minimum','maximum','major_tick','minor_tick'))
    extra=2
    max_num=3

    formfield_overrides = {
        models.FloatField: {'widget': TextInput(attrs={'style':'width:80px'})},
    }

class GraphAdmin(admin.ModelAdmin):
    model=Graph
    fields=('title','isTable')
    inlines=[ImageInlineGraph,GraphAxisInline]


class GraphContainerInline(admin.TabularInline):
    model=GraphContainer
    fields=('graph',)



####### FORMULA CATEGORY #######

class CategoryAliasInline(admin.TabularInline):
    model = CategoryAlias

class CategoryAdmin(admin.ModelAdmin):
    model = FormulaCategory
    inlines = [CategoryAliasInline]

####### FORMULA #######

class FormulaModelForm( forms.ModelForm ):
    summary = forms.CharField( widget=forms.Textarea, required=False )
    class Meta:
        model = Formula

class FormulaAdmin(admin.ModelAdmin):
    form = FormulaModelForm
    model = Formula
    list_display=['name','category']

    filter_horizontal = ('sympy_symbols','nomenclature_symbols',)
    inlines=[ImageInlineFormula,GraphContainerInline]
    exclude=('sympy_vars','tags')
    fields=('name','category','summary','nomenclature_symbols','sympy_equation','sympy_symbols','legal')


####### SYMBOL #######

class SymbolAdmin(admin.ModelAdmin):
    model=Symbol


####### UNIT #######

class UnitAdmin(admin.ModelAdmin):
    model=Unit
    #exclude=('compound',)
    list_display=['multiplicand']


class UnitFormSet(BaseInlineFormSet):
       def get_queryset(self):
        if not hasattr(self, '_queryset'):
            qs = super(UnitFormSet, self).get_queryset().filter(compound=False)
            self._queryset = qs
        return self._queryset

class UnitInline(admin.TabularInline):
    model = Unit
    formset = UnitFormSet
    exclude=('compound',)
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'style':'width:80px'})},
        models.FloatField: {'widget': NumberInput(attrs={'style':'width:70px'})}
    }

class CompoundUnitFormSet(BaseInlineFormSet):
       def get_queryset(self):
        if not hasattr(self, '_queryset'):
            qs = super(CompoundUnitFormSet, self).get_queryset().filter(compound=True)
            self._queryset = qs
        return self._queryset

class CompoundUnitInline(UnitInline):
    formset = CompoundUnitFormSet
    extra = 0
    verbose_name_plural = "Created Units"
    max_num = 0 # disables adding units
    

####### FEEDBACK #######

class FeedBackAdmin(admin.ModelAdmin):
    model=FeedBack
    readonly_fields=('summary',)


####### ACKNOWLEDGEMENTS #######

class AckAdmin(admin.ModelAdmin):
    model=Ack


####### COMPOUND CATEGORIES #######

class CompoundConversionRowForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
	super(CompoundConversionRowForm, self).__init__(*args, **kwargs)
	w = self.fields['units'].widget
	choices = []
	if hasattr(self.instance,'category'):
	    cat_pk = self.instance.category.pk
	    wtf = UnitCategory.objects.get(pk=cat_pk).units_set.all()
	    for choice in wtf:
	        choices.append((choice.id, choice.name))
	w.choices = choices


class CompoundConversionRowInline(admin.TabularInline):
    fk_name = 'parent'
    extra = 0
    model = CompoundConversion
    fields= ('category','exponent','units')
    filter_horizontal = ('units',)
    form = CompoundConversionRowForm
    verbose_name_plural = "Build Compound Units"
    verbose_name = "Build Component"
    formfield_overrides = {
	models.IntegerField: {'widget': NumberInput(attrs={'style':'width:60px'})}
    }

class CompoundContainerForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
	super(CompoundContainerForm, self).__init__(*args, **kwargs)
	w = self.fields['compound'].widget
	choices = []
	if hasattr(self.instance,'category'):
	    cat_pk = self.instance.category.pk
	    wtf = UnitCategory.objects.get(pk=cat_pk).compound_cat_set.all()
	    for choice in wtf:
	        choices.append((choice.id, choice.__unicode__()))
	w.choices = choices
 
class CompoundCategoryContainerInline(admin.TabularInline):
    model = CompoundCategoryContainer
    verbose_name_plural = "Compound Categories"
    extra = 1
    form = CompoundContainerForm

class CompoundCategoryRowInline(admin.TabularInline):
    model = CompoundCategoryRow  
    formfield_overrides = {
	models.IntegerField: {'widget': NumberInput(attrs={'style':'width:60px'})}
    }

class CompoundCategoryAdmin(admin.ModelAdmin):
    model = CompoundCategory
    inlines = [CompoundCategoryRowInline]
    verbose_name_plural = "Unit Category Compounds"
    list_display=['__unicode__','category']


####### UNIT CATEGORY #######

class UnitCategoryAdmin(admin.ModelAdmin):
    inlines=[CompoundCategoryContainerInline,UnitInline,CompoundUnitInline,CompoundConversionRowInline]
    fields= ('name',('isDimensionless','hidden'))


####### STEAM TABLES #######

class SteamParametersInline(admin.TabularInline):
    model = SteamParameter
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'style':'width:120px'})}
    }

class SteamCalculationsInline(admin.TabularInline):
    model = SteamCalculation


class SteamTableAdmin(admin.ModelAdmin):
    model=SteamTable
    inlines=[SteamParametersInline,SteamCalculationsInline,GraphContainerInline]




####### REGISTER #######

admin.autodiscover()
admin.site.register(Formula,FormulaAdmin)
admin.site.register(UnitCategory,UnitCategoryAdmin)
admin.site.register(Symbol,SymbolAdmin)
admin.site.register(FeedBack,FeedBackAdmin)
admin.site.register(Ack,AckAdmin)
admin.site.register(Graph,GraphAdmin)
admin.site.register(SteamTable,SteamTableAdmin)
admin.site.register(Image,ImageAdmin)
admin.site.register(FormulaCategory,CategoryAdmin)
admin.site.register(CompoundCategory,CompoundCategoryAdmin)
