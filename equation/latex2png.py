from subprocess import call,PIPE
import os
import string
import random
from django.core.files.base import File

#DPI=['163','326','132','264']
DPI=['326']
def Latex2Png(equation,location=''):
    print equation
    if location:
        url=location 
    else:
        url='/tmp/'
    if not os.path.isdir(url):
        os.makedirs(url)
    
    
    contents=r'''\documentclass[varwidth=true,border=1pt]{standalone}
    \usepackage{float}
    \usepackage{lscape}		% To allow landscape tables
    \usepackage{makeidx}		% For MakeIndex
    \usepackage{amsmath}
    \usepackage{hvmath}
    \usepackage{chngcntr}	% Defines counterwithin and counterwithout
    \usepackage{array}
    \usepackage{tabularx}
    \usepackage{fancyhdr}
    %\usepackage{pdfsync}
    \usepackage{siunitx}		% Formats standard SI units
    \usepackage{multirow}
    %\usepackage{supertabular}
    \usepackage{calc}		% Required by longtable
    \usepackage{longtable}
    \usepackage{ctable}
    \DeclareMathOperator*\sech  {sech}
    \DeclareMathOperator*\cosec  {cosec}
    \DeclareMathOperator*\var  {var}
    \DeclareMathOperator*\se  {se}
    \DeclareMathOperator*\bias  {bias}
    \DeclareMathOperator*\sd  {sd}
    \DeclareMathOperator*\MSE  {MSE}
    \begin{document}
    \begin{equation*}'''+equation+r'''\end{equation*}
    \end{document}'''

    fileName=''.join(random.choice(string.ascii_uppercase + string.digits) for x in xrange(5))

    pdfFilePath=url+fileName+'.pdf'
    pngFilePath=url+fileName+'.png'
    texFilePath=url+fileName+'.tex'
    text_file = open(texFilePath, "w")
    text_file.write(contents)
    text_file.close()


    with open(os.devnull, "w") as fnull:
        print ' '.join(['pdflatex','-output-directory='+url[:-1], texFilePath])
        print ' '.join(['convert' ,'-density' ,DPI[0] ,pdfFilePath, '-quality', '90' ,pngFilePath])
        call(['pdflatex','-output-directory='+url[:-1], texFilePath], stdout = fnull, stderr = PIPE)
#        for res in DPI:
        call(['convert' ,'-density' ,DPI[0] ,pdfFilePath, '-quality', '90', '-resize','1300x>' ,pngFilePath], stdout = fnull, stderr = PIPE)

        


    png= File(open(pngFilePath))
    os.unlink(texFilePath)
    os.unlink(pdfFilePath)
    os.unlink(pngFilePath)
    return png
