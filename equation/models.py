import io
from django.db import models
from smart_selects.db_fields import ChainedForeignKey
from wand.image import Image as wandFile
from os import path
# from datetime 
import datetime
from django.utils.timezone import utc
from equation.latex2png import Latex2Png
from django.core.exceptions import ValidationError
from django.core.files.base import File
import os
from sympy import latex
from tempfile import TemporaryFile


# Create your models here.

####### ACK #######

class Ack(models.Model):
    text = models.CharField(max_length=5000)

    class Meta:
        verbose_name_plural = 'Acknowledgements'


####### FORMULA CATEGORY #######

class FormulaCategory(models.Model):
    name = models.CharField(max_length=20)
    modified = models.DateTimeField(auto_now=True)

    def syncDict(self):
        als = []
        for a in self.aliases.all():
            als.append(a.name)
        sd = {
            'pk': self.pk,
            'name': self.name,
            'alias': als
        }
        return sd

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Formula Categories'


class CategoryAlias(models.Model):
    name = models.CharField(max_length=20)
    category = models.ForeignKey('FormulaCategory', related_name='aliases', null=True)

    class Meta:
        verbose_name_plural = 'Category aliases'


####### FORMULA #######

class Formula(models.Model):
    name = models.CharField(max_length=500)
    category = models.ForeignKey('FormulaCategory', related_name='formulae')
    summary = models.CharField(max_length=1500, blank=True)
    modified = models.DateTimeField(auto_now=True)
    sympy_equation = models.CharField(max_length=3000, blank=True)
    sympy_vars = models.CharField(max_length=500, blank=True)
    tags = models.CharField(max_length=2500, blank=True)
    legal = models.CharField(max_length=2500, blank=True)
    sympy_symbols = models.ManyToManyField("Symbol", blank=True, related_name='formula_sympy_set')
    nomenclature_symbols = models.ManyToManyField("Symbol", blank=True, related_name='formula_nomenclature_set')

    def getVariables(self):
        temp = []
        for f in self.sympy_symbols.all():
            temp.append(f.sympy)
        return temp

    def __init__(self, *args, **kwargs):
        super(Formula, self).__init__(*args, **kwargs)

    def syncDict(self):
        sym = []
        for s in self.sympy_symbols.all():
            sym.append(s.pk)

        nom = []
        for n in self.nomenclature_symbols.all():
            nom.append(n.pk)
            im = []
            for i in self.images.all():
                im.append(i.pk)

        gph = []
        for g in self.formula_graphs_container.all():
            if hasattr(g, 'graph'):
                gph.append(g.graph.pk)

        sd = {
            'pk': self.pk,
            'name': self.name,
            'category': self.category.pk,
            'summary': self.summary,
            'nomenclature': nom,
            'legal': self.legal,
            'symbols': sym,
            'images': im,
            'graphs': gph,
            'tags': self.tags,
        }

        return sd

    def save(self, *args, **kwargs):
        super(Formula, self).save(*args, **kwargs)
        tgs = ""
        for n in self.nomenclature_symbols.all():
            if n.name:
                tgs = tgs + n.name + ", "
        for i in self.images.all():
            if i.name:
                tgs = tgs + i.name + ", "
        self.tags = tgs  # ' '.join([self.category.name,self.name,self.summary])
        print "formula pk ", self.pk
        super(Formula, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Formulae'


####### IMAGE #######

class Image(models.Model):
    img = models.FileField(upload_to='images/', blank=True)
    latex = models.CharField(max_length=2000, blank=True)
    modified = models.DateTimeField(auto_now=True)
    main = models.BooleanField(default=False)
    formula = models.ForeignKey('Formula', related_name='images', null=True)
    name = models.CharField(max_length=100, blank=True)
    index = models.IntegerField(default=0)
    graph = models.OneToOneField('Graph', related_name='image', null=True)

    __original_latex = None

    def __init__(self, *args, **kwargs):
        super(Image, self).__init__(*args, **kwargs)
        self.__original_latex = self.latex

    def clean(self, *args, **kwargs):
        if not self.img:
            return
        filename = self.img.name
        ext = os.path.splitext(filename)[1]
        ext = ext.lower()
        if ext not in ['.pdf', '.png', '.jpg', '.jpeg']:
            raise ValidationError('File must be a pdf or png')
        if ext in ['.pdf', '.jpg', '.jpeg']:
            tpath = '.'.join(filename.split('.')[:-1]) + '.png'
            buffer = TemporaryFile()
            with wandFile(file=self.img, resolution=264) as img:
                img.trim(color=None, fuzz=0)
                img.save(file=buffer)
                buffer.seek(0)
                self.img = File(buffer)
                self.img.name = tpath

    def save(self, *args, **kwargs):

        if (self.__original_latex != self.latex and self.latex) or not self.img:
            self.img = Latex2Png(self.latex)
        super(Image, self).save(*args, **kwargs)
        self.__original_latex = self.latex

    def syncDict(self):
        sd = {
            'pk': self.pk,
            'image': self.img.url[1:],
            'main': self.main,
            'name': self.name,
            'index': self.index
        }
        return sd

    def __unicode__(self):
        if self.formula:
            return self.formula.name + " - " + self.name
        if hasattr(self, 'graph'):
            return self.graph.title + " - " + self.name
        return self.name


####### UNIT CATEGORY #######

class UnitCategory(models.Model):
    name = models.CharField(max_length=100)
    isDimensionless = models.BooleanField(default=False)
    hidden = models.BooleanField(default=False)
    modified = models.DateTimeField(auto_now=True)

    def syncDict(self):
        unitSet = []
        for u in self.units_set.all():
            unitSet.append(u.pk)
        compoundArray = []
        for container in self.compound_categories_set.all():
            containerArray = []
            for c in container.compound.rows_set.all():
                containerArray.append([c.category.pk, c.exponent])
            compoundArray.append(containerArray)
        sd = {'name': self.name,
              'units': unitSet,
              'isDimensionless': self.isDimensionless,
              'pk': self.pk,
              'hidden': self.hidden,
              'compoundParams': compoundArray
              }
        return sd

    def save(self, *args, **kwargs):
        super(UnitCategory, self).save(*args, **kwargs)

        # use the save function to programatically generate units from the compound categories

        # check that all created compound components have units
        can_calculate = True
        for compound in self.compound_children_set.all():
            if compound.units.count() is 0:
                can_calculate = False

        # if all components have units then carry on, else do nothing
        if can_calculate:
            # get array of unit arrays, ready to create compound units from 
            units_array = []
            compounds = self.compound_children_set.all()
            for compound in compounds:
                units = compound.units.all()
                if len(units_array) is 0:
                    for unit in units:
                        units_array.append([unit])
                else:
                    new_array = []
                    for saved_units in units_array:
                        for unit in units:
                            unit_set = saved_units + [unit]
                            new_array.append(unit_set)
                    units_array = new_array

            # for each array of units, generate a unit compound object
            for units in units_array:
                unit_name = ""
                unit_plural = ""
                unit_display = ""
                unit_multiplicand = 1
                unit_addend = 0

                had_plural = False
                for i in range(0, len(units)):
                    unit = units[i]
                    exponent = compounds[i].exponent

                    base_plural_name = ""
                    is_last = i is len(units) - 1
                    if had_plural:
                        base_plural_name = unit.getName()
                    else:
                        if is_last:
                            base_plural_name = unit.getPluralName()
                        else:
                            next_exp = compounds[i + 1].exponent
                            if next_exp < 0:
                                had_plural = True
                                base_plural_name = unit.getPluralName()
                            else:
                                base_plural_name = unit.getName()
                    poly_dict = {0: "", 1: "", 2: "square ", 3: "cubic ", 4: "quartic ", 5: "quintic "}
                    poly = poly_dict[abs(exponent)]
                    prefix = ""
                    if exponent < 0:
                        prefix = "per "

                    if unit_name != "":
                        unit_name = unit_name + " "
                    if unit_plural != "":
                        unit_plural = unit_plural + " "

                    unit_name = unit_name + prefix + poly + unit.getName()
                    unit_plural = unit_plural + prefix + poly + base_plural_name
                    unit_multiplicand = unit_multiplicand * pow(unit.multiplicand, exponent)
                    unit_display = unit_display + unit.getDisplay()

                    # if the compound unit has not used a plural name and the last component unit has a shorthand for the plural name, use that for clarity
                    if is_last and not had_plural:
                        if unit.plural_name[0] == "+":
                            unit_plural = unit.plural_name

                    if exponent == 1:
                        if not is_last:
                            unit_display = unit_display + " "
                    elif exponent != 0:
                        unit_display = unit_display + "^{" + str(exponent) + " }"

                new_unit = Unit.objects.create(name=unit_name, plural_name=unit_plural, units=unit_display,
                                               multiplicand=unit_multiplicand, addend=unit_addend, category=self,
                                               compound=True)
                new_unit.save()

            # clean units from compounds once created
            for compound in compounds:
                compound.units.clear()

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Unit Categories'
        ordering = ('name',)


class CompoundConversion(models.Model):
    parent = models.ForeignKey('UnitCategory', related_name='compound_children_set')
    category = models.ForeignKey('UnitCategory', related_name='compound_rows_set')
    exponent = models.IntegerField(default=1)
    units = models.ManyToManyField('Unit', related_name='compound_units_set', null=True, blank=True)


class CompoundCategoryContainer(models.Model):
    category = models.ForeignKey('UnitCategory', related_name='compound_categories_set')
    compound = models.ForeignKey('CompoundCategory', related_name='compounds_set')


class CompoundCategory(models.Model):
    category = models.ForeignKey('UnitCategory', related_name='compound_cat_set')

    def __unicode__(self):
        title = ""
        for row in self.rows_set.all():
            if title != "":
                title = title + " x "
            title = title + row.category.name
            if row.exponent is not 0 and row.exponent is not 1:
                title = title + "^" + str(row.exponent)
        return title

    class Meta:
        verbose_name_plural = 'Compound Categories'


class CompoundCategoryRow(models.Model):
    compound = models.ForeignKey('CompoundCategory', related_name='rows_set')
    category = models.ForeignKey('UnitCategory', related_name='compound_row_set')
    exponent = models.IntegerField(default=1)


####### UNIT #######

class Unit(models.Model):
    name = models.CharField(max_length=200)
    plural_name = models.CharField(max_length=200, default='+s', blank=True)
    distinction = models.CharField(max_length=200, default='', blank=True)
    units = models.CharField(max_length=50, blank=True)
    multiplicand = models.FloatField(default=1)
    addend = models.FloatField(default=0)
    isBaseUnit = models.BooleanField(default=False)
    category = models.ForeignKey('UnitCategory', related_name='units_set')
    modified = models.DateTimeField(auto_now=True)

    compound = models.BooleanField(default=False)

    def syncDict(self):
        sd = {'name': self.name,
              'plural': self.plural_name,
              'distinction': self.distinction,
              'units': self.units,
              # 'compound':self.compound,
              'multiplicand': self.multiplicand,
              'addend': self.addend,
              'isBaseUnit': self.isBaseUnit,
              'pk': self.pk,
              }
        return sd

    def getName(self):
        if self.distinction:
            return self.name + " (" + self.distinction + ")"
        else:
            return self.name

    def getPluralName(self):
        returnstring = ""
        if self.plural_name.isspace() or self.plural_name is "":
            returnstring = self.name
        elif self.plural_name[0] == "+":
            returnstring = self.name + self.plural_name[1:]
        else:
            returnstring = self.plural_name

        if self.distinction:
            return returnstring + " (" + self.distinction + ")"
        else:
            return returnstring

    def getDisplay(self):
        if self.units:
            return self.units
        else:
            return self.getPluralName()

    def __unicode__(self):
        return self.name


####### SYMBOL #######

class Symbol(models.Model):
    name = models.CharField(max_length=50)
    sympy = models.CharField(max_length=300)
    display = models.CharField(max_length=50)
    unitCategory = models.ForeignKey('UnitCategory')
    onlyReal = models.BooleanField(default=True)
    modified = models.DateTimeField(auto_now=True)
    constant = models.FloatField(default=0)
    constant_unit = ChainedForeignKey(
        Unit,
        chained_field='unitCategory',
        chained_model_field='category',
        show_all=False,
        auto_choose=True,
        null=True,
        blank=True,
    )
    isConstant = models.BooleanField(default=False)

    def syncDict(self):
        sd = {'name': self.name,
              'onlyReal': self.onlyReal,
              'category': self.unitCategory.pk,
              'pk': self.pk,
              'sympy': self.sympy,
              'isConstant': self.isConstant,
              'constant': self.constant,
              'display': self.display
              }
        if self.constant_unit is not None:
            sd['constantUnit'] = self.constant_unit.pk
        return sd

    def __unicode__(self):
        if self.onlyReal:
            return self.name + ' - ' + self.sympy + ' ' + self.unitCategory.name
        return self.name + ' - ' + self.sympy + ' ' + self.unitCategory.name + ' imaginary'


####### FEEDBACK #######

class FeedBack(models.Model):
    summary = models.CharField(max_length=5000)

    def __unicode__(self):
        return self.summary

    class Meta:
        verbose_name_plural = 'Feedback'


####### STEAM TABLES #######

class SteamParameter(models.Model):
    table = models.ForeignKey('SteamTable', related_name='parameter_set', null=True, default=1)
    name = models.CharField(max_length=50, blank=True)
    category = models.ForeignKey('UnitCategory', related_name='category_set', null=True, default=None)
    default_unit = ChainedForeignKey(
        Unit,
        chained_field='category',
        chained_model_field='category',
        show_all=False,
        auto_choose=True,
        related_name='unit_set',
        null=True
    )
    key = models.CharField(max_length=50)

    def __unicode__(self):
        return self.category.name


class SteamCalculation(models.Model):
    table = models.ForeignKey('SteamTable', related_name='calculation_set')
    parameter_1 = models.ForeignKey('SteamParameter', related_name='param_1_set')
    parameter_2 = models.ForeignKey('SteamParameter', related_name='param_2_set')

    def __unicode__(self):
        return self.parameter_1.key + "," + self.parameter_2.key


class SteamTable(models.Model):
    modified = models.DateTimeField(auto_now=True)

    def syncDict(self):
        gph = []
        for g in self.steam_graphs_container.all():
            if hasattr(g, 'graph'):
                gph.append(g.graph.pk)
        params = {}
        for p in self.parameter_set.all():
            params[p.key] = p.default_unit.pk
        calcs = []
        for c in self.calculation_set.all():
            st = c.parameter_1.key + "," + c.parameter_2.key
            calcs.append(st)
        sd = {
            'pk': self.pk,
            'graphs': gph,
            'parameters': params,
            'calculations': calcs
        }
        return sd

    def __unicode__(self):
        return 'Steam Table'

    class Meta:
        verbose_name_plural = 'Steam Tables'


####### GRAPH #######

class GraphAxis(models.Model):
    modified = models.DateTimeField(auto_now=True, default=datetime.date.today)
    graph = models.ForeignKey('Graph', related_name='axes_set')
    title = models.CharField(max_length=100)
    AXIS_TYPE_CHOICES = ((0, "x-axis"), (1, "y-axis"), (2, "secondary y-axis"))
    axis_type = models.IntegerField(choices=AXIS_TYPE_CHOICES, default=0)
    category = models.ForeignKey('UnitCategory', related_name='axis_set')
    unit = ChainedForeignKey(
        Unit,
        chained_field='category',
        chained_model_field='category',
        show_all=False,
        auto_choose=True,
        related_name='axis_set'
    )
    maximum = models.FloatField(default=1)
    minimum = models.FloatField(default=0)
    major_tick = models.FloatField(default=1)
    minor_tick = models.FloatField(default=1)
    logarithmic = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Axes'

    def syncDict(self):
        sd = {
            'pk': self.pk,
            'name': self.title,
            'type': self.axis_type,
            'unit': self.unit.pk,
            'max': self.maximum,
            'min': self.minimum,
            'major': self.major_tick,
            'minor': self.minor_tick,
            'log': self.logarithmic,
        }
        return sd

    def clean(self, *args, **kwargs):
        if not self.maximum > self.minimum:
            raise ValidationError('The maximum value of the axis must be larger than the minimum!')

    def __unicode__(self):
        return self.title


class GraphContainer(models.Model):
    formula = models.ForeignKey('Formula', related_name='formula_graphs_container', null=True, blank=True)
    steam_tables = models.ForeignKey('SteamTable', related_name='steam_graphs_container', null=True, blank=True)
    graph = models.ForeignKey('Graph', related_name='container', null=True)

    def __unicode__(self):
        if hasattr(self, 'graph'):
            return self.graph.title
        return "Graph"

    class Meta:
        verbose_name = 'Graph'
        verbose_name_plural = 'Graphs'


class Graph(models.Model):
    title = models.CharField(max_length=100)
    isTable = models.BooleanField(default=False)
    modified = models.DateTimeField(auto_now=True, default=datetime.date.today)

    def __unicode__(self):
        return self.title

    def syncDict(self):
        axes = []
        for a in self.axes_set.all():
            axes.append(a.pk)

        sd = {
            'pk': self.pk,
            'name': self.title,
            'image': self.image.pk,
            'axes': axes
        }
        return sd

    class Meta:
        verbose_name = 'Graph'
        verbose_name_plural = 'Graphs'
