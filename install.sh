#!/bin/bash
echo 'updating apt-get'
apt-get update
echo 'installing pip'
apt-get install python-pip
echo 'installing memcached'
apt-get install memcached
echo 'installing postgresql'
apt-get install postgresql
echo 'installing scipy'
apt-get install python-scipy

echo 'installing requirements'
apt-get install python-pip python-dev build-essential
apt-get install libpq-dev uwsgi-core tmux
apt-get install nginx
apt-get install git
echo 'pulling project'
#git clone https://pruthvikar@bitbucket.org/pruthvikar/data-and-formulae-server.git
echo 'installing requirements'
pip install -r requirements.txt 

echo 'create database'

###   NEW  ###
apt-get install texlive #LaTeX
apt-get install imagemagick #needed for converting pdf to png
