import re
import pprint
text='''base
			Length & metre & m \\
			Mass & kilogram & kg \\
			Time & second & s \\
			Electric current & ampere & A \\
			Thermodynamic temperature & kelvin & K \\
			Luminous intensity & candela & cd \\

derived
			Acceleration, linear & $\text{metre}/\text{second}^2$ & \SI{}{m.s^{-2}} \\
			Acceleration, angular & $\text{radian}/\text{second}^2$ & \SI{}{rad.s^{-2}} \\
			Area & $\text{metre}^2$ & \SI{}{m^2} \\
			Density & $\text{kilogram} / \text{metre}^3$ & \SI{}{kg.m^{-3}} \\
			Force & newton & N (= \SI{}{kg.m.s^{-2}})\\
			Frequency & hertz & (Hz = \SI{}{s^{-1}}) \\
			Impulse, linear & newton-second & \SI{}{N.s} \\
			Impulse, angular & newton-metre-second & \SI{}{N.m.s} \\
			Moment of force & newton-metre & \SI{}{N.m} \\
			Second moment of area & $\text{metre}^4$ & \SI{}{m^4} \\
			Moment of inertia &  $\text{kilogram-metre}^2$ & \SI{}{kg.m^2} \\
			Momentum, linear & $\text{kilogram-metre}/\text{second}$ & \SI{}{kg.m.s^{-1}} \\
			Momentum, angular & $\text{kilogram-metre}^2/\text{second}$ & \SI{}{kg.m^2.s^{-1}} \\
			Power & watt & W (= \SI{}{J.s^{-1}} = \SI{}{N.m.s^{-1}} \\
			Pressure, stress & pascal & Pa (= \SI{}{N.m^{-2}}) \\
			Stiffness (linear), spring constant & $\text{newton}/\text{metre}$ & \SI{}{N.m^{-1}} \\
			Velocity, linear & $\text{metre}/\text{second}$ & \SI{}{m.s^{-1}} \\
			Velocity, angular & $\text{radian}/\text{second}$ & \SI{}{rad.s^{-1}} \\
			Volume & $\text{metre}^3$ & \SI{}{m^3} \\
			Work, energy & joule & J (= \SI{}{N.m})\\

electric
			Potential & volt & V (= \SI{}{W.A^{-1}}) \\
			Resistance & ohm & $\upomega$ (= \SI{}{V.A^{-1}})\\
			Charge & coulomb & C (= \SI{}{A.s}) \\
			Capacitance & farad & F (= \SI{}{A.s.V^{-1}}) \\
			Electric field strength & $\text{volt}/\text{metre}$ & \SI{}{V.m^{-1}} \\
			Electric flux density & $\text{coulomb}/\text{metre}^2$ & \SI{}{C.m^{-2}} \\
			
magnetic
            Magnetic flux & weber & Wb (= \SI{}{V.s}) \\
			Inductance & henry & H (= \SI{}{V.s.A^{-1}}) \\
			Magnetic field strength & --- &  \SI{}{A.m^{-1}}  \\
			Magnetic flux density & --- & \SI{}{Wb.m^{-2}} \\
'''
unitsDict={}
for i in text.splitlines(): 
    
    if i:
        
        buf= i.replace('\t','').replace('\\','').split('&')
        if len(buf)==3:
            unitsDict[buf[0]]={'quantity':buf[1],'symbol':buf[2]}
for i in unitsDict.keys():
    print ' '.join(list(reversed(i.lstrip().rstrip().split(',')))).title().lstrip().rstrip()