Django #==1.6.1
python-memcached>=1.53
django-extensions>=1.3.3
psycopg2>=2.5.2
uwsgi
sympy
django-suit
#django-smart-selects
# currently have to install django-smart-selects from git as the pypi version is not updated for Django 1.7
git+https://github.com/digi604/django-smart-selects.git@master
django-selectable
#-e git+git://github.com/Soaa-/django-nested-inlines.git#egg=django-nested-inlines
iapws
