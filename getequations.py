from equation.models import Formula,FeedBack
from django.http import HttpResponse
from django.core.files.base import ContentFile
import re
from django.views.decorators.csrf import csrf_exempt
from equation.latex2png import Latex2Png as l2p
from django.core.files.base import File
from equation.eqndets import DETAILS
import json

def getFromBook(request):

    linestring = open('Databook13.tex', 'r').readlines()
    lines=[]
    inEQN=''
    num=0
    Equations={}
    sec=''
    subsec=''
    subsubsec=''
    for i,line in enumerate(linestring):
        if '\section' in line:
            sec=line[9:-2]
        elif '\subsection'in line:
            subsec=line[12:-2]
        elif '\subsubsection' in line:
            subsubsec=line[15:-2]
            
        if inEQN:
            if 'equation' in line and 'end' in line:
                inEQN=False
            elif 'includegraphics' in line:
                continue
            else:
                Equations[num]['eqn']+=line.lstrip('\t')

        else:
            if 'equation' in line and 'begin' in line:
                num+=1
                Equations[num]={}
                Equations[num]['eqn']=''
                Equations[num]['sec']=sec
                Equations[num]['subsec']=subsec
                Equations[num]['subsubsec']=subsubsec
                inEQN=True
            
    print len(Equations.keys())
    c=0
    for k,v in Equations.iteritems():

        c+=1
        eqn=l2p(v['eqn'],c)



        return
        name = v.get('sec')
        f=Formula(category=v.get('sec'),name=v.get('subsubsec'))
        fd=FormulaDetails(raw_equation=v.get('eqn'),equation='',summary=v.get('subsec'),formula=f)
        
        fd.save()
        
        f.save()
        fd.equation.save(
            eqn.split('/')[-1],
            File(open(eqn))
            )
        fd.formula=f
        f.formuladetails=fd
        fd.save()
        f.save()
    return HttpResponse('Created entries')
  
@csrf_exempt
def sendFeedback(request):
    sum=request.POST.get('summary')
    if sum:
        FeedBack(summary=sum).save()
        return HttpResponse(json.dumps({'status':'Feedback Saved'}), content_type="application/json")
    return HttpResponse(json.dumps({'status':'Not received'}), content_type="application/json")
